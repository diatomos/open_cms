<?php
/**
 * Created by PhpStorm.
 * User: azizharazi
 * Date: 30/09/2019
 * Time: 17:01.
 */

namespace App\Entity;

class Product
{
    private $price;

    public function __construct($price)
    {
        $this->price = $price;
    }

    public function getPrice()
    {
        return $this->price;
    }
}
