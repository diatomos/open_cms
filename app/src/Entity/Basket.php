<?php
/**
 * Created by PhpStorm.
 * User: azizharazi
 * Date: 30/09/2019
 * Time: 17:01.
 */

namespace App\Entity;

class Basket
{
    private $products = [];

    public function __construct()
    {
    }

    public function add(Product $product)
    {
        $this->products[] = $product;
    }

    public function price()
    {
        $price = 0;
        foreach ($this->products as $product) {
            $price += $product->getPrice();
        }

        return $price;
    }
}
