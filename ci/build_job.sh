DOCKER_TAG=`md5sum ./docker/Dockerfile | awk '{ print $1 }'`

docker pull registry.gitlab.com/diatomosteam/cms/php:$DOCKER_TAG

if [ $? -ne 0 ]; then
docker build ./docker --target php  -t registry.gitlab.com/diatomosteam/cms/php:$DOCKER_TAG
docker push registry.gitlab.com/diatomosteam/cms/php:$DOCKER_TAG
fi


docker pull registry.gitlab.com/diatomosteam/cms/php:$DOCKER_TAG

if [ $? -eq 0 ]; then
    exit;
fi

docker build ./docker --target nginx  -t registry.gitlab.com/diatomosteam/cms/nginx:$DOCKER_TAG
docker push registry.gitlab.com/diatomosteam/cms/nginx:$DOCKER_TAG 
