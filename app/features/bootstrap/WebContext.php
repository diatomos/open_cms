<?php
/**
 * Created by PhpStorm.
 * User: azizharazi
 * Date: 01/10/2019
 * Time: 12:35
 */

use Behat\MinkExtension\Context\MinkContext;
use Behat\Behat\Tester\Exception\PendingException;

class WebContext extends  MinkContext
{

    /**
     * @When I wait for :arg1 seconds
     */
    public function iWaitForSeconds($arg1)
    {
        $this->getSession()->wait($arg1 * 1000);
    }

}