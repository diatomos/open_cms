.PHONY: help install_vendor install_node_modules install_database build_assets install_dist
.DEFAULT_GOAL = help

ENCORE_ENV?= dev
COM_COLOR   = \033[0;34m
OBJ_COLOR   = \033[0;36m
OK_COLOR    = \033[0;32m
ERROR_COLOR = \033[0;31m
WARN_COLOR  = \033[0;33m
NO_COLOR    = \033[m

help: 
	@grep -E '(^[a-zA-Z_-]+:.*?##.*$$)|(^##)' $(MAKEFILE_LIST) | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[32m%-10s\033[0m %s\n", $$1, $$2}' | sed -e 's/\[32m##/[33m/'

##php cs fixer on test
############
##   GIT
############
install_dist: ## install env and dist files
	cp .env.dist .env && cp docker-compose.yml.dist docker-compose.yml && cp app/.env.dist app/.env && cp app/.php_cs.dist app/.php_cs && cp app/phpunit_myapp.xml.dist app/phpunit_myapp.xml

##############
##  COMPOSER
##############
app/composer.lock: app/composer.json ## Lancer composer update si composer.json est modifié
	echo -e "mise à jours $(OK_COLOR)des vendors$(NO_COLOR)" 
	docker-compose exec php composer update

app/vendor: app/composer.lock ## Lancer composer install si composer.lock est modifié
	echo -e "installation des $(OK_COLOR)vendors composer$(NO_COLOR)" 
	docker-compose exec php composer install

install_vendor: app/vendor app/composer.lock ## Lance l'installation des vendors composer


####################
##  WEBPACK ENCORE
####################
app/node_modules: app/package.json
	echo -e "installation des $(OK_COLOR)modules nodes$(NO_COLOR)" 
	docker-compose exec php yarn install 

install_node_modules: app/node_modules app/yarn.lock ## Lance l'installation des modules node

build_assets: ## Lance le build webpack
	echo -e "lancement des $(OK_COLOR)builds$(NO_COLOR)" 
	rm -rf app/public/build
	docker-compose exec php yarn run encore $(ENCORE_ENV) 

##############
##  DATABASE
##############

install_database: ## installation de la base de données
	docker-compose exec php bin/console doctrine:database:create --if-not-exists && docker-compose exec php bin/console doctrine:schema:update --force

##############
## CS FIXER 
##
## command lines
## docker-compose exec php php-cs-fixer fix -v --dry-run src
## docker-compose exec php php-cs-fixer fix src tests -v  --config=.php_cs
##############
#app/php-cs-fixer:
#	docker-compose exec php curl -L https://cs.symfony.com/download/php-cs-fixer-v2.phar -o php-cs-fixer
#	docker-compose exec php chmod a+x php-cs-fixer
#	docker-compose exec php ln -s php-cs-fixer /usr/local/bin/php-cs-fixer

install_cs_fixer: app/php-cs-fixer ## install php cs fixer
	cp .php_cs.dist .php_cs


#############
## BEHAT 
##
## command lines
## docker-compose exec php composer vendor/bin/behat --init
## docker-compose exec php ./vendor/bin/behat
## docker-compose exec php ./vendor/bin/behat -dl
##############


#############
## PHP UNIT
##
## commande lines
## docker-compose exec php php bin/phpunit tests/Controller
#############


###############
##  GITLAB
##
##  Docker registry
##  docker login registry.gitlab.com
##  docker tag nginx:latest registry.gitlab.com/diatomosteam/cms/nginx-diatomos:latest
##  docker push registry.gitlab.com/diatomosteam/cms/nginx-diatomos:latest
###############
