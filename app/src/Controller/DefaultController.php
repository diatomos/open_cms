<?php
/**
 * Created by PhpStorm.
 * User: azizharazi
 * Date: 04/07/2019
 * Time: 17:52.
 */

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DefaultController extends AbstractController
{
    /**
     * @Route("", name="default_index", methods="GET")
     */
    public function indexAction(): Response
    {
        return $this->render('front/index.html.twig');
    }

    /**
     * @Route("post_contact_form", name="post_contact_form", methods="POST")
     *
     * @param Request             $request
     * @param TranslatorInterface $translator
     * @param \Swift_Mailer       $mailer
     *
     * @return Response
     */
    public function SendFormAction(Request $request, TranslatorInterface $translator, \Swift_Mailer $mailer): Response
    {
        $status = 200;
        $errorField = [];

        $formData = $request->request->all();
        $message = $translator->trans('form.message.success');

        foreach ($formData as $key => $value) {
            if (null === $value || '' === $value) {
                $errorField[$key] = $translator->trans('form.message.error.empty');
                $message = $translator->trans('form.message.error.all');
            } else {
                if ('email' === $key) {
                    if (!filter_var(mb_strtolower($value), FILTER_VALIDATE_EMAIL)) {
                        $errorField[$key] = $translator->trans('form.message.error.email_format');
                    }
                }
            }
        }

        if (0 === count($errorField)) {
            try {
                $mailMessage = (new \Swift_Message('Hello Email'))
                    ->setFrom('harazi.a@gmail.com')
                    ->setTo($this->getParameter('admin_email'))
                    ->setBody(
                        $this->renderView(
                        // templates/emails/registration.html.twig
                            'email/contactForm.html.twig',
                            ['formData' => $formData]
                        ),
                        'text/html'
                    );

                $mailer->send($mailMessage);
            } catch (\Exception $e) {
                $status = 500;
                $message = $translator->trans('form.message.error.internal_server');
            }
        }

        return new JsonResponse(
            ['response' => ['status' => $status, 'error' => $errorField, 'message' => $message],
            ]
        );
    }
}
