<?php
/**
 * Created by PhpStorm.
 * User: azizharazi
 * Date: 30/09/2019
 * Time: 17:39
 */

use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use App\Entity\Basket;
use App\Entity\Product;

class BasketContext implements Context
{

    /**
     * @var Basket $basket
     */
    private $basket;

    /**
     * @Given An empty basket
     */
    public function anEmptyBasket()
    {
        $this->basket = new Basket();
    }

    /**
     * @Then The basket price is :arg1 $
     */
    public function theBasketPriceIs($arg1)
    {
        if($this->basket->price() != $arg1) {
            throw new Exception('Le prix ne correspond pas');
        }
    }

    /**
     * @Given A product costing :arg1 $ is adding to the basket
     */
    public function aProductCostingIsAddingToTheBasket($price)
    {
        $product = new Product($price);
        $this->basket->add($product);
    }

}