<?php
/**
 * Created by PhpStorm.
 * User: azizharazi
 * Date: 10/07/2019
 * Time: 19:39.
 */

namespace App\Controller\Back;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/admin")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("", name="admin_index", methods="GET")
     */
    public function indexAction()
    {
        return $this->render('back/index.html.twig');
    }
}
