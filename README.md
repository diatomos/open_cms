# Diatomos CMS Project (Symfony 4)

Installation :
git clone --single-branch --branch develop git@gitlab.com:diatomosteam/cms.git

[![coverage](https://gitlab.com/diatomos/cms/badges/master/coverage.svg?job=coverage)]

[![build status](https://gitlab.com/diatomosteam/cms/badges/master/build.svg)](https://gitlab.com/diatomosteam/cms/commits/master)
